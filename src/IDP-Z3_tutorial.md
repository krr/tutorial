IDP-Z3 How-to guides
====================

This document contains how-to guides for IDP-Z3.
IDP-Z3 is a software collection implementing the Knowledge Base paradigm using the FO[·] language.
FO[·] facilitates the development of knowledge-intensive applications capable of intelligent behavior {cite}`carbonnelle2022idpz3`.

The document is meant to be used by Knowledge engineers.

Suggestions for improvements to the document can be made via [issues on GitLab](https://gitlab.com/krr/IDP-Z3/-/issues).


```{toctree}
---
   maxdepth: 1
   caption: Contents
---
Vocabulary
Vocabulary2
biblio
genindex
```

