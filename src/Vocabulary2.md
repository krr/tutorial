
How-to choose a vocabulary (v2)
==========================

## 1. Find out what the theory is about.

Explanation: we shall have to write a theory consisting of a set of definitions and propositions in terms of  a selection of well-chosen concepts,  that  describe the aspects of the application domain that are relevant for the series of tasks that need to be achieved with it.

In the running example of this tutorial: : the end  goal of this application is a K-based tool that assists us in concluding legal purchases of  a  house, potentially optimised to the wishes of the buyer and/or seller involved. Purchases are or may be complex composite objects. So the theory will be about all aspects describing the purchase and about all the features of the world that may have influence on whether a  purchase is legal.  In addition, the theory may be about concepts that affect buyer and seller, e.g., the taxes that the buyer must expect to pay yearly, once (s)he is in  possession of the house. This is what the theory will be about.

## 2. Find sources of information about what the theory is about.

IN the running example: find legal text  describing the legally correct ways of concluding a correct sale of a single house. Find a human domain expert that can explain details and help you fine tune what the theory is to be about.  Find specification documents of the domain and the sort of tasks to be performed. This is the sort of sources of information to be searched for.

For this tutorial, we provide legal text, annotated with comments of the notary. E.g., the notary may say that a certain type of purchase discussed in  article such or so needs not to be supported by the system.  In that, that article can be skipped.

## 3. Determine the ontology

Determine the relevant type and relational concepts, needed to express the knowledge.

Explanation: In the end, we will build a logic theory consisting of definitions of terminology, and propositions that describe the purchase and and in what circumstances it is legal. We begin by determining  a class of type  and relational concepts on the application domain in terms of which we can express these definitions and propositions. Take care that the concepts are elementary and cannot be split up. E.g., Do not use a relational concept relating a first name, with a second name, with a street name with a street number, with a city name and a zIP code, and .... Instead introduce elementary types for a Person, a name, an address,  and relational concepts such as the first name of a person, the family name, and the address of a person; for the address of a person, add relational concepts to indicate its street, number, city; relational concepts for its city: for its ZIP code and for its name and for the country.

Running example:   we go over the legislation text and documents describing the desires of buyers and sellers, while underlining keywords and phrases that represent important concepts.  We underline a term in red the first time we meet a specific concept, and in black all next times. Whenever a new concept is detected, we add it to the list of concepts, plus information about whether it is an atomic type, or a relational concept, and in the latter case, the number of arguments, the type of the arguments,  the multiplicity of arguments, and above all: the meaning  of  the concept.

## 4. Choose a symbolic notation for all concepts.

Explanation: we choose type symbols for types and predicate symbols for relational concepts. If the relational symbols have a multiplicity of 1 on one argument (meaning tat

Explanation: this returns a formal  typed vocabulary consisting of type symbols, identifiers, typed predicate and functions symbols,  together with a clear informal interpretation of each symbol, namely the domain specific concept that is attached to it. This allows us to interpret the formal definitions and propositions obtained in the fifth step, as meaningful statements about the application domain.
