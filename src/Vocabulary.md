How to choose a vocabulary (v1)
==========================

Choosing an appropriate vocabulary is a critical step in building a knowledge base for IDP-Z3.
This guide presents a method to perform this step successfully.

In summary, the method consists of the following steps:

1. identify the purpose of the knowledge base;
2. identify **sample elementary facts** that can be used to describe a **state of affairs**;
3. identify all the relational concepts, i.e., the **relations** between **entiti(es)** of adequate **type**;
4. identify **base types** and **subtypes**;
5. identify **mandatory and uniqueness constraints**;
6. choose **identifiers** for base types;
7. (optional) identify **singleton** types;
8. declare predicates and functions.

We'll illustrate the method using the "real estate transfer tax" law of Flanders,
i.e., the law that specifies the tax on transfer of ownership (i.e., sales) of real estate.
Our goal is to represent the knowledge of that law in FO[·] in order to build a tax advisor for that law.


## 1. Identify the purpose of the knowledge base

A knowledge base supports many reasoning tasks, of varying complexity:
relevance determination, propagation, optimisation, explanation, ...
When designing the vocabulary, it is easiest to address the simplest reasoning task, "model checking".
Model checking is the task of deciding whether a fully-known state of affairs satisfies a set of conditions set forth in the knowledge base.
The answer is binary, i.e., yes or no.

So, the first step in our methodology is to identify the elementary, binary question about state of affairs
that the knowledge base should be able to answer in model checking.
Usually, this question is one of the following:

* is this fully-known state of affairs possible ?
* is this fully-known state of affairs acceptable ?
* is this fully-known state of affairs desirable ?

For a system reasoning about the law, the question usually is: "is this fully-known state of affairs acceptable, i.e., lawful?".
In our tax example, a state of affairs is lawful when the proper tax has been paid in time.

A theory represents the set of conditions to be satisfied by a state of affairs
so that the answer to the question is "yes".
All the conditions must be satisfied for a "yes" answer: the theory is a conjunction ("and") of the conditions.
For example, a state of affairs is lawful when all the articles of applicable laws are satisfied.

<!--
The question "is the state of affairs **unlawful**?" is not a good binary question,
because the answer is "yes" if any one of the conditions expressed in the law is not satisfied (the theory would need to be a disjunction, "or")
-->

In some cases, the knowledge base is meant to be used to answer several binary questions.<!--For our tax example, the knowledge base should help decide if a state of affairs is *desirable*, i.e., if it minimizes the tax amount due.-->
The knowledge base then consists of a set of theories, one for each binary question.
Each theory will have a vocabulary, possibly extending the vocabulary used by another theory.
For example, because what is acceptable must also be possible,
the "acceptable?" theory will extend the "possible?" theory with additional conditions,
possibly involving additional information.
As a consequence, the vocabulary of the "acceptable?" theory will extend the vocabulary of the "possible?" theory.


## 2. Sample elementary facts in English

To describe a fully-known state of affairs, one uses **elementary facts**,
i.e., sentences such as "The value of the house is 300K euros.",  or "The tax amount due is 36K euros."
In the first step, we collect a good samples of elementary facts, by reading the law and/or talking with a tax expert and its clients.

The sample sentences should be:

* relevant to answer the binary question(s) determined in step 1;
* present and observable in states of affairs;
* elementary, and yet unambiguous.

"the amount is X" is ambiguous, because it does not clearly states
whether X is the value of the house or the amount due.

This step is informal and may miss some important information.
The following steps will refine our understanding of the vocabulary.
The method proposed here is iterative.

## 3. Identify the relational concepts

IDP-Z3 is not capable of understanding English directly.
Instead, it uses a formal language with a limited vocabulary and grammar.
At this stage, we seek to develop a minimal vocabulary that allows us
to express the knowledge in a way understandable by IDP-Z3.

Because English is such a rich language,
there are many different ways to express the same information.
For our example, the following sentences express the same information:

* "The value of the transfer of ownership A is 300K euros."
* "The price in contract A is 300K euros."
* "300K is the amount stated in the contract between seller S and buyer B on date D.

In order to keep the knowledge base simple, only one of the formulations should be kept.
Which one?
This step seeks to address that question.

<!--Our practice shows that this question cannot be answered for each sentence in isolation;
instead, the full set of "fact types" must be considered.-->

Essentially, each of the sentences above expresses a relation between "300K euros"
and a sales contract, identified by different means (we'll talk about **identifiers** in a later steps).
"300K euros" and the contract are 2 **objects** in the domain of discourse,
each having a different **type** and playing a different **role** in the **relation**.

In practice, a relation may involve 1, 2 or more objects.
Such relations can be represented in a tabular format, with one column per type.
For our example (again ignoring the different ways to identify the sales contract):

```{list-table} Table 1: The "..is the price in.." relation
:header-rows: 1
:name: Price

* - Amount
  - Sales Contract
* - 300K
  - Sales Contract A
* - 200K
  - Sales Contract B
```

The caption of the table contains the template of the English sentence (aka, the "informal semantics" of the relation),
the headings contain the types of the objects in the relation, and the rows contain the objects in the relation.
The table does not have duplicate rows.

The other relation we need for our legal application is:

```{list-table} Table 2: The "..is due for.." relation
:header-rows: 1
:name: Tax

* - Amount
  - Sales Contract
* - 36K
  - Sales Contract A
* - 24K
  - Sales Contract B
```

Let's assume that these two relational concepts provide enough details for our purpose.
If they were insufficient, we would seek additional English sentences in the law (or by talking with an expert)
that would convey the missing information;
from these sentences, we would then identify the missing relation(s) as explained in this step.

Optionally, our vocabulary can be represented graphically as follows:

```{figure} _static/ORM-1.png
---
height: 150px
name: ORM-1
---
Fig.1: A graphical representation of our vocabulary
```

Ellipses are used to represent types, and edges between ellipses are used to represent relations.
(The ellipse of "Amount" has a dotted line because it is a value: we won't have to come up with identifiers for it)
Edges are annotated with the informal semantics of the relation,
and have as many cells as they are types involved in the relation.
(You may recognize the [Object-Role Model (ORM) notation](https://en.wikipedia.org/wiki/Object-role_modeling))

As we'll see later, relations correspond to predicates (or functions) in a FO[.] vocabulary.

## 4. Identify base types and subtypes

We human tend to see types as part of a hierarchy.
For example, `cat` and `dog` are subtypes of `animal`.

In this step, we draw the hierarchy of the types used in our vocabulary.
We also determine if the vocabulary can be simplified by replacing relations involving subtypes
by one relation involving a super-type.

For example, we might have a unary relation "..walks" for cats, and another for dogs.
We may realize that "..walks" is actually a relation for `animal` instead.

Optionally, the type hiearchy can be represented in a diagram using arrows:

```{figure} _static/ORM-hierarchy.png
---
height: 150px
name: ORM-hierarchy
---
Fig.2: A type hierarchy example
```

In our simple legal example, there is no subtype, so we can skip this step.

## 5. Mandatory and uniqueness constraints

<!--In the legal example, it is common sense that a sales contract has only one (total) value,
from which the tax due can be computed by applying the tax rate.-->

In this step, we identify the uniqueness and mandatory constraints of each relation.
This is useful to decide whether to represent a relation as a predicate, a total function, or a partial function.
Indeed, unlike a predicate, a function has an implicit uniqueness constraint:
a function applied to some arguments can only have one value.

A **uniqueness constraint** exists when some objects occur at most once in a relation, in any states of affairs.
For example, there can only be one tax amount due for a sales contract.

The uniqueness constraints can be easily determined by looking at the tabular representation of the relation:
can a column (or combination of columns) contain the same object (or combination of objects) several times ?
(Remember that the table does not have duplicate rows)

In our legal example, a contract can only occur once in the "..is the price in.." relation.
This is shown by chosing the sales contract as the first column, and adding the double-line border after the first column in the table below:

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:1px solid #E1E4E5;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:1px solid #E1E4E5;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  ;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0pky{border-color:#E1E4E5;text-align:left;vertical-align:top}
</style>
<table class="tg">
<thead>
  <tr>
    <td class="tg-0pky" style="font-weight:bold">Sales contract</th>
    <td class="tg-0pky" style="padding: 10px 2px"></th>
    <td class="tg-0pky">Amount</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">Sales contract A</td>
    <td class="tg-0pky" style="padding: 10px 2px"></td>
    <td class="tg-0pky">300K</td>
  </tr>
  <tr>
    <td class="tg-0pky">Sales contract B</td>
    <td class="tg-0pky" style="padding: 10px 2px"></td>
    <td class="tg-0pky">200K</td>
  </tr>
</tbody>
</table>
</br>

Optionally, we can represent that information by adding a horizontal line above the second cell of the relation:

```{figure} _static/ORM-uniqueness.png
---
height: 150px
name: ORM-uniqueness
---
Fig.3: Uniqueness and mandatory constraints
```

This step can help us further simplify our vocabulary:
if a ternary relation has only one column with a uniqueness constraint,
there is a good chance that the relation should be split in 2 binary relations.
In our legal example, we could have identified the ternary relational concept:
"X and Y are the price and amount due in Contract C",
instead of the 2 binary facts above.
This ternary relation has a uniqueness constraint on the Contract role:
hence, we should consider splitting it in the 2 relations we used in the previous section.
The 2 binary facts are more elementary than the ternary fact, and should be preferred.

The second part of this step is to identify the **mandatory constraints** in a relation.
This is useful to distinguish total functions from partial functions.
Again, this is done by considering the tabular form of the relation:
is it mandatory for each object in a type to occur in the table ?
(In more complex cases, we also ask: is it mandatory for each object in a type to occur in **one of** a set of relations.
See the [ORM White Paper](http://www.orm.net/pdf/ORMwhitePaper.pdf) for details)

In our legal case, it is clear that each sales contract must mention a price
(otherwise, we could not compute the tax due).
This is represented in the table above by using a bold font for the "Sales Contract" heading.
In Fig. 3, this is represented by adding a black dot on the "Sales Contract" type,
for each relation that it must have.

When a type has a unique constraint but not a mandatory constraint in a relation,
one might check whether a subtype should be used in the relation instead:
the subtype would be chosen so that it has a mandatory constraint on the relation.

## 6. Choose identifiers for base types

We now return to the question of "how to identify the objects in a type".
So far, we have used letters A, B, X...
We could also use strings (e.g., 'John Doe').
In many cases, this is adequate.

In the FO[.] vocabulary, the `SalesContract` type would be declared as follows:

```
type SalesContract := {A, B, C}
```

In other cases, one may use a **constructor** instead.
A constructor is used to encode a descriptive phrase,
e.g., "the contract between buyer A and Seller B signed on date D".
Such descriptive phrase uniquely identifies an object in the domain of discourse.
In the FO[.] vocabulary, this would be represented as follows:

```
type Person := {A, B, C}
type SalesContract := constructed from {contract(seller: Person, buyer: Person, signed_on:Date)}
```

Notice that Contract has a unique and mandatory constraint
on the "..is buyer in..", "..is seller in.." and "..signed on date.." relations:
all such relations are candidate parts of the constructor.

While the base types are declared using the `type` keyword,
subtypes are declared using unary predicates, e.g.,:

```
dog, cat: Animal -> Bool
```

## 7. (optional) Identify singleton types

This step is closely related to the previous step, and is optional.

In some cases, the knowledge base is meant to be used to answer binary questions
about states of affairs containing only one object of some types.
For example, our tax advisor only needs to consider one Sales Contract at a time:
the `SalesContract` type is a singleton in the states of affairs:
its extension only contains the sales contract under consideration.

This can be used to simplify the vocabulary: instead of talking of "the tax amount due for sales contract A",
one can just talk about "the tax amount due".
On the other hand, one may want to keep the more complete vocabulary to avoid rework should the purpose change.

## 8. Declare predicates and functions

The last step to create the vocabulary is to declare predicates and symbols for the relations.

Each relation is represented by a predicate or a function (depending on its uniqueness constraints).
The name of the predicate or functions should be related to the informal semantics of the relation.
Their signature includes the types (or subtypes) having a role in the relation.

In our legal example, the following declarations could be made:

```
price, due : SalesContract -> Real
```

If the `SalesContract` type is a singleton, one could use instead:

```
price, due : () -> Real
```


